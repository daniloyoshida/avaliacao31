
import utfpr.ct.dainf.pratica.Poligonal;
import utfpr.ct.dainf.pratica.PontoXZ;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica {

    public static void main(String[] args) {
        PontoXZ p1 = new PontoXZ(-3, 2);
        PontoXZ p2 = new PontoXZ(-3, 6);
        PontoXZ p3 = new PontoXZ(0, 2);
        Poligonal poli = new Poligonal();
        poli.getVertices().add(p1);
        poli.getVertices().add(p2);
        poli.getVertices().add(p3);
        try{
            System.out.println(poli.getComprimento());
        }
        catch(RuntimeException aa){
            aa.getLocalizedMessage();
        }
    }
    
}
