
package utfpr.ct.dainf.pratica;



public class PoligonalFechada extends Poligonal<Ponto2D>{

    @Override
    public double getComprimento() {
        Ponto2D res[];
        int i=0;
        res = new Ponto2D[getVertices().size()];
        double res2 = 0;
        for(Ponto2D a: getVertices()){
//            res[i] = Math.sqrt(Math.pow(a.getX(), 2) + Math.pow(a.getY(), 2) + Math.pow(a.getZ(), 2));
            res[i].setX(a.getX());
            res[i].setY(a.getX());
            res[i].setZ(a.getX());
        }
        for (int j=0; j<i-1; j++)
            res2 += Math.sqrt(Math.pow((res[j].getX()-res[j+1].getX()), 2) + 
                    Math.pow((res[j].getY()-res[j+1].getY()), 2) + 
                    Math.pow((res[j].getZ()-res[j+1].getZ()), 2));
        res2+= Math.sqrt(Math.pow((res[i].getX()-res[0].getX()), 2) + 
                    Math.pow((res[i].getY()-res[0].getY()), 2) + 
                    Math.pow((res[i].getZ()-res[0].getZ()), 2));
        
        return res2;
    }
    
}
